app.bind('default', function (context) {

  const $navPages = $('[js-nav-pages]', context)

  $('[js-nav-tabs]', context).each(function () {
    const $navTabs = $(this)
    const navTabsId = $navTabs.data('group')
    const $navGroup = $navPages.filter('[data-group=' + navTabsId + ']')
    const $navTabsItem = $navTabs.find('[js-nav-tabs-item]')
    const $navGroupItems = $navGroup.find('[js-nav-pages-item]')

    let currentActiveID
    let oldActiveID

    $navTabsItem.on('click.tabs', function (e) {
      e.preventDefault()
      const $root = $(this)

      oldActiveID = $navTabsItem.filter('.is-active').data('id')
      currentActiveID = $root.data('id')

      $navTabsItem.removeClass('is-active')
      $root.addClass('is-active')

      $navGroupItems.removeClass('is-active').filter('[data-id=' + currentActiveID + ']').addClass('is-active')

      switch (navTabsId) {
        case 'projects':
          $root.closest('.screen').removeClass(`screen_project-${oldActiveID}`)
          $root.closest('.screen').addClass(`screen_project-${currentActiveID}`)
          break
        case 'cases':
          $root.closest('.screen').removeClass(`screen_case-${oldActiveID}`)
          $root.closest('.screen').addClass(`screen_case-${currentActiveID}`)
          break
      }
    })
  })
})