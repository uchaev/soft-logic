(function ($) {
    $.fn.inputShadow = function () {
        return $(this).each(function (parent_i, parent) {
            var $parent = $(parent);
            var $input = $parent.find('input, textarea, [contenteditable], select');
            var isRequired = $input.prop('required') || false;
            var isContenteditable = $input.is('[contenteditable]');
            var errorRemoveEvent = $parent.data('error-remove');

            if (isContenteditable) {
                var $hiddenInput = $('<input type="text" name="' + $input.attr('name') + '" value="' + $input.html().trim().replace(/<div>/gi, '<br>').replace(/<\/div>/gi, '').replace(/<br>/gi, '\n') + '"/>').appendTo($parent);
                $hiddenInput.hide().prop({
                    disabled: $input.attr('disabled'),
                    required: $input.attr('required'),
                });
            }

            $parent.removeClass('is-focus is-filled').toggleClass('is-required', isRequired);
            $input.off('.input-shadow');

            if ($input.is('select')) {
                var $options = $input.find('option');
                var selectedVal;
                $input.on({
                    'update.input-shadow': function (e) {
                        $options = $input.find('option');
                        $input.trigger('changeSilent');
                    },
                    'change.input-shadow changeSilent.input-shadow': function (e, notUser) {
                        if ($input.is(':disabled')) {
                            $parent.addClass('is-disabled');
                        }
                        else {
                            $parent.removeClass('is-disabled');
                        }

                        selectedVal = $options.filter(':selected').val();

                        if (selectedVal !== '' && selectedVal !== '00') {
                            $parent.addClass('is-filled');

                            if (!notUser && $parent.hasClass('is-error')) {
                                $parent.removeClass('is-error');
                            }
                        }
                        else {
                            $parent.removeClass('is-filled');

                            if (!notUser && !isRequired && $parent.hasClass('is-error')) {
                                $parent.removeClass('is-error');
                            }
                        }
                    }
                });
            }
            else if ($input.is(':checkbox, :radio')) {
                var group = $input.data('fieldset');
                var $group = group ? $parent.parents('[js-inputShadow-fieldset][data-fieldset="'+ group +'"]').off('.input-shadow') : $();
                var $groupInputs = $group.length ? $group.find('input') : $();
                $input.on({
                    'change.input-shadow': function (e) {
                        if ( $.isEmptyObject($input.inputValidate()) ) {
                            $parent.removeClass('is-error');
                            $input.data({valid: true}).trigger('valid');
                        }
                        else {
                            $parent.addClass('is-error');
                            $input.data({valid: false}).trigger('invalid');
                        }
                    }
                });
                if ( $input.is(':radio') ) {
                    var name = $input.attr('name');
                    $group.on({
                        'valid.input-shadow': function (e) {
                            $groupInputs.filter(`[name="${name}"]`).data({valid: true});
                        },
                        'invalid.input-shadow': function (e) {
                            $groupInputs.filter(`[name="${name}"]`).data({valid: false});
                        }
                    });
                }
                $group.on({
                    'valid.input-shadow invalid.input-shadow': function (e) {
                        if ( $groupInputs.filter(function (inputIndex, input) {return $groupInputs.eq(inputIndex).data('valid') === false}).length ) {
                            $group.addClass('is-error');
                        }
                        else {
                            $group.removeClass('is-error');
                        }
                    }
                });
            }
            else {
                $input.on({
                    'clear.input-shadow': function (e) {
                        // e.stopPropagation();
                        $parent.removeClass('is-error is-ok is-valid');
                        $input.val(null).trigger('changeSilent');
                    },
                    'focus.input-shadow': function (e) {
                        $parent.addClass('is-focus');
                        if (errorRemoveEvent == 'focus') {
                            $parent.removeClass('is-error');
                        }
                        if ( $.fn.inputmask && $input.data('inputmask-showmaskonfocus') && $input.inputmask("hasMaskedValue") ) {
                            $parent.addClass('is-filled');
                        }
                    },
                    'blur.input-shadow': function (e) {
                        var $targetInput = isContenteditable ? $hiddenInput : $input;
                        $parent.removeClass('is-focus');
                        if ( !$.isEmptyObject($targetInput.inputValidate()) ) {
                            $parent.removeClass('is-ok is-valid');
                            $targetInput.data({valid: false}).trigger('invalid');
                        }
                        else{
                            $parent.addClass('is-valid');
                            $targetInput.data({valid: true}).trigger('valid');
                        }
                    },
                    'keyup.input-shadow': function (e) {
                        if (isContenteditable) {
                            $input.trigger('changeSilent');
                        }
                    },
                    'input.input-shadow change.input-shadow changeSilent.input-shadow': function (e, notUser) {
                        // $parent.removeClass('is-autofilled');
                        if ($input.is(':disabled')) {
                            $parent.addClass('is-disabled');
                        }
                        else {
                            $parent.removeClass('is-disabled');
                        }

                        if ($input.val().length > 0 || (isContenteditable && $input.text().length > 0) || ( $.fn.inputmask && $input.inputmask("unmaskedvalue").length > 0)) {
                            $parent.addClass('is-filled');
                            if ($parent.hasClass('is-error') && e.type !== 'change' && !notUser) {
                                $parent.removeClass('is-error');
                            }
                        }
                        else if ( ($.fn.inputmask && !$input.inputmask("hasMaskedValue")) ) {
                            $parent.removeClass('is-filled');
                        }

                        if (e.type !== 'change' && !notUser) {
                            $parent.removeClass('is-ok is-valid');
                        }

                        if (isContenteditable) {
                            $hiddenInput.val($input.html().trim().replace(/<div>/gi, '<br>').replace(/<\/div>/gi, '').replace(/<br>/gi, '\n'));
                        }

                        if ( notUser && $.fn.inputmask && $input.inputmask("unmaskedvalue").length == 0 ) {
                            $parent.removeClass('is-filled');
                        }
                    },
                    'maskIncomplete': function (e) {
                        $input.inputValidate();
                        if ( $input.inputmask("unmaskedvalue").length == 0 ) {
                            $parent.removeClass('is-filled');
                        }
                    }
                });
            }

            $input.trigger('changeSilent', true);
        });
    };
})(jQuery);
