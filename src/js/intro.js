app.bind('default', function (context) {
  const TRANSITION_TIME = 456

  const $introNav = $('[js-intro-nav]', context)
  const $introNavNext = $introNav.find('[js-intro-nav-next]')
  const $introNavPrev = $introNav.find('[js-intro-nav-prev]')
  const $introNavTitle = $introNav.find('[js-intro-nav-title]')
  const $introNavCountCurrent = $introNav.find('[js-intro-nav-count-current]')
  const $introNavVideos = $introNav.find('[js-intro-nav-video]')

  const $introSlider = $('[js-intro-slider]', context)
  const $introSliderItems = $introSlider.find('[js-intro-slider-item]')

  // const introSliderTimeout = $introSlider.data('timeout') * $introSliderItems.length

  function changeSlide (direction) {
    const $currentActiveItem = $introSliderItems.filter('.is-active')
    let $changeActiveItem
    switch (direction) {
      case 'next' :
        $changeActiveItem = $currentActiveItem.next().length > 0 ? $currentActiveItem.next() : $introSliderItems.eq(0)
        break
      case 'prev' :
        $changeActiveItem = $currentActiveItem.prev().length > 0 ? $currentActiveItem.prev() : $introSliderItems.eq($introSliderItems.length - 1)
        break
    }
    const changeActiveItemInfo = JSON.parse(JSON.stringify($changeActiveItem.data('info')))
    const nextActiveItemInfo = $changeActiveItem.next().length > 0 ? JSON.parse(JSON.stringify($changeActiveItem.next().data('info'))) : JSON.parse(JSON.stringify($introSliderItems.eq(0).data('info')))

    $introNavTitle.html(nextActiveItemInfo.title)
    $introNavCountCurrent.html(changeActiveItemInfo.pos)
    $introNavNext.css('background-color', nextActiveItemInfo.color)
    $introNavPrev.css('background-color', changeActiveItemInfo.color_prev)
    $introNavVideos.removeClass('is-active').filter('[data-id=' + nextActiveItemInfo.id + ']').addClass('is-active')

    $currentActiveItem.addClass('is-animate-hidden')
    const $video = $changeActiveItem.find('video')
    const src = $video.data('src')
    if (isDesktop()) {
      $video.attr('src', src)
    }
    setTimeout(() => {
      $currentActiveItem.removeClass('is-active is-animate-hidden')
      if (isDesktop()) {
        if ($video.length) {

          let promise = $video[0].pause()

          if (promise !== undefined) {
            promise.then(_ => {}).catch(error => {})
          }
        }
      }
      $changeActiveItem.addClass('is-active')
      if (isDesktop()) {
        if ($video.length) {

          let promise = $video[0].play()

          if (promise !== undefined) {
            promise.then(_ => {}).catch(error => {})
          }
        }
      }
    }, TRANSITION_TIME)
  }

  $introSlider.on('next.slider', function () {
    changeSlide('next')
  })

  $introSlider.on('prev.slider', function () {
    changeSlide('prev')
  })

  $introNavNext.on({
    click () {
      $introSlider.trigger('next.slider')
      let $video = $introNavNext.find('[js-intro-nav-video]').filter('.is-active').find('video')
      if (isDesktop()) {
        $video.attr('src', $video.data('src'))
        let promise = $video[0].play()
        if (promise !== undefined) {
          promise.then(_ => {}).catch(error => {})
        }
      }
    },
    mouseover () {
      let video = $introNavNext.find('[js-intro-nav-video]').filter('.is-active').find('video')[0]
      if (isDesktop()) {
        let promise = video.play()

        if (promise !== undefined) {
          promise.then(_ => {}).catch(error => {})
        }
      }
    },
    mouseout () {
      $introNavNext.find('video').each(function () {
        if (isDesktop()) {
          let promise = this.play()
          if (promise !== undefined) {
            promise.then(_ => {}).catch(error => {})
          }
          this.play()
        }
      })
    }
  })

  $introNavPrev.on({
    click () {
      $introSlider.trigger('prev.slider')
    }
  })

  function isDesktop () {

    if ($(window).width() >= 1200) {
      return true
    }

    return !deviceType.mobile
  }
})