app.bind('default', (context) => {
    $('[js-technology-grid]', context).each(function () {
        const $grid = $(this);
        $grid.masonry({
            // options
            itemSelector: '[js-technology-grid-item]',
            columnWidth: '[js-technology-grid-item]',
            gutter: 10
        });
    })
});
