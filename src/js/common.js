app.pluralize = (n, titles) => `${n} ${titles[n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2]}`

app.isInViewport = ($element, edge) => {
  let rect = (($element instanceof jQuery) ? $element[0] : $element).getBoundingClientRect()
  return (rect.bottom >= 0 && rect.right >= 0 && rect.top <= window.innerHeight && rect.left <= window.innerWidth)
}

app.bind('default layer', function (context) {
  $('[js-overlayer]', context).layer({
    isOverlayer: true,
    beforeOpen: ($layer, layerData) => {
    },
    afterOpen: ($layer, layerData) => {
      app.use('basic', $layer)
    },
    afterClose: ($layer, layerData) => {
    }
  })
})

app.bind('default inputs basic', function (context) {
  $('[js-selectus]', context).selectus()

  $('[js-phonemask]', context).attr('data-is-inputmask', true).inputmask({
    mask: '+7 (999) 999-99-99',
    placeholder: '',
    jitMasking: 4,
    showMaskOnHover: false,
    showMaskOnFocus: false,
    isComplete: function (buffer, opts) {
      return Inputmask.isValid(buffer.join(''), { mask: opts.mask })
    },
    oncomplete: function () {
      $(this).trigger('maskComplete')
    },
    onincomplete: function () {
      $(this).trigger('maskIncomplete')
    }
  })
  $('[js-input-digital]', context).inputmask(
    {
      regex: '[0-9]*',
      showMaskOnHover: false,
      showMaskOnFocus: false,
    })
  $('[js-inputShadow]', context).inputShadow()
  $('[js-input-number]', context).attr('data-is-inputmask', true).each((inputIndex, input) => {
    const $input = $(input)
    let minValue = typeof $input.data('min') !== 'undefined' ? parseInt($input.data('min')) : $input.prop('min').length > 0 ? $input.prop('min') : 1
    let maxValue = typeof $input.data('max') !== 'undefined' ? parseInt($input.data('max')) : $input.prop('max').length > 0 ? $input.prop('max') : 999999999
    let value = parseInt($input.val().replace(/[^0-9.]/g, ''))

    $input.off('.input-number').on({
      'input.input-number': (e) => {
        setValue()
      },
    }).inputmask({
      mask: '9{*}',
      repeat: maxValue.length,
      greedy: true,
      regex: '[0-9]*',
      showMaskOnHover: false,
      showMaskOnFocus: false,
      isComplete: function (buffer, opts) {
        return Inputmask.isValid(buffer.join(''), { regex: opts.regex })
      }
    })

    function setValue (val = parseInt($input.val().replace(/[^0-9.]/g, ''))) {
      if (val < minValue) {
        value = minValue
        $input.val(value).trigger('change')
      }
      else if (val > maxValue) {
        value = maxValue
        $input.val(value).trigger('change')
      }
      else {
        value = val
      }

      return value
    }
  })
  $('[data-inputmask]', context).attr('data-is-inputmask', true).inputmask({
    oncomplete: function () {
      $(this).trigger('maskComplete')
    },
    onincomplete: function () {
      $(this).trigger('maskIncomplete')
    }
  })
})

app.bind('default burger', function (context) {
  let $btn = $('[js-header-menu]', context)
  let $btnClose = $('[js-header-close]', context)

  $btn.on('click', function () {
    Layer.open('main/menu')
  })

  $btnClose.on('click', function () {
    Layer.close('main/menu', {})
  })
})

app.bind('default toggler basic', function (context) {
  $('[js-toggler]', context).toggler()
})

app.bind('default', function (context) {
  $('[js-menu-open]', context).on('click', function () {
    Layer.open('main/menu')
  })
})

app.bind('default auth', function (context) {
  $('[js-auth-open]', context).each(function () {
    let $root = $(this)
    $root.on('click', function (e) {
      e.preventDefault()
      Layer.open('main/auth')
    })
  })

  $('[js-reg-open]', context).each(function () {
    let $root = $(this)
    $root.on('click', function (e) {
      e.preventDefault()
      Layer.open('main/reg')
    })
  })

  $('[js-recovery-open]', context).each(function () {
    let $root = $(this)
    $root.on('click', function (e) {
      e.preventDefault()
      Layer.open('main/recovery')
    })
  })

})

app.bind('default', function (context) {
  const $input = $('[js-input-payment]', context)
  const $togglers = $('[js-toggle-payment]', context)

  $togglers.on('change', function () {
    $input.val('')
  })

  $input.on('blur', function () {
    if ($input.val() > 0) {
      $togglers.each(function () {
        $(this).prop('checked', false)
      })
    }
    else if ($input.val() < 1 && $input.val().trim() !== '') {
      $input.val(10)
      $togglers.each(function () {
        $(this).prop('checked', false)
      })
    }
  })
})

app.bind('default orders-actions', function (context) {
  $('[js-card]', context).each(function () {
    const $card = $(this)
    const cardID = $card.data('id')
    const $links = $card.find('[js-card-order-link]')
    $links.each(function (indexLink, link) {
      const $link = $(link)
      const layerID = $link.data('layer')
      $link.on('click', function () {
        Layer.open(layerID, { id: cardID })
      })
    })
  })
})

app.bind('default', function () {
  $('[js-button-reload]').on('click', function () {
    $(this).addClass('is-load')
  })
})

app.bind('default', function (context) {

  $('[js-callback-menu]', context).on('click', function () {
    $(this).closest('[js-menu]').find('[js-menu-side]').addClass('is-callback')
  })

  $('[js-callback-menu-close]', context).on('click', function () {
    $(this).closest('[js-menu]').find('[js-menu-side]').removeClass('is-callback')
  })
})

app.bind('default', function (context) {
  const $window = $(window)
  const $document = $(document)
  const $link = $('[js-scroll-top]', context)
  $window.on({
    scroll: function () {
      if ($document.scrollTop() > $window.height()) {
        $link.addClass('is-active')
      }
      else {
        $link.removeClass('is-active')
      }
    }
  })
  $link.on('click', function () {
    $.scrollTo('body', 300)
  })
})