app.bind('default', function (context) {
  $('[js-articles-slider]', context).each(function () {
    const $articles = $(this)
    const $slider = $articles.find('[js-articles-slider-list]')
    const $sliderNav = $articles.find('[js-articles-slider-nav]')

    $slider.addClass('owl-carousel').owlCarousel({
      loop: false,
      nav: false,
      dots: false,
      navContainer: $sliderNav,
      margin: 30,
      navText: [],
      items: 3,
      responsive: {
        0: {
          items: 1,
        },
        500: {
          items: 2,
        },
        1050: {
          items: 3
        }
      },
      onInitialized () {
        $articles.addClass('is-init')
      },
      onChange () {
        $slider.addClass('is-lock')
      },
      onChanged () {
        $slider.removeClass('is-lock')
      }
    })

  })
})