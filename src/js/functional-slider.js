app.bind('default', function (context) {
    $('[js-bank-functuional-slider]', context).each(function () {
        const $articles = $(this)
        const $slider = $articles.find('[js-bank-functuional-slider-list]')
    
        $slider.addClass('owl-carousel').owlCarousel({
            loop: false,
            nav: false,
            dots: false,
            margin: 30,
            navText: [],
            items: 3,
            responsive: {
            0: {
                items: 1,
            },
            500: {
                items: 2,
            },
            1000: {
                items: 3
            }
            },
            onInitialized () {
                $articles.addClass('is-init')
            },
            onChange () {
                $slider.addClass('is-lock')
            },
            onChanged () {
                $slider.removeClass('is-lock')
            }
        })
  
    })
})