$(function () {
  var $window = $(window);

  var $item = $('[js-paralax]');

  var option = {
    verticalParallax: true,
    positionProperty: 'transform',
    responsive: true,
    motionType: 'gaussian',
    mouseMotionType: 'gaussian',
    motionAngleX: 50,
    motionAngleY: 70,
    useGyroscope: false,
    alphaFilter: 0.5,
    adjustBasePosition: true,
    alphaPosition: 0.025
  };

  if ($item.length) {
    $window.ready(function () {
      if ($window.width() <= 1200) {
        $.parallaxify('destroy');
        $('[data-parallaxify-range-x]').removeAttr('style');
        $('[data-parallaxify-range-y]').removeAttr('style');
      } else {
        $.parallaxify(option);
      }
    });
    $window.on('resize', function () {
      if ($window.width() <= 1200) {
        $.parallaxify('destroy');
        $('[data-parallaxify-range-y]').removeAttr('style');
        $('[data-parallaxify-range-x]').removeAttr('style');
      } else {
        $.parallaxify(option);
      }
    });
  }
});