app.bind('default', function (context) {

  const $map = $('[js-map-contact]', context)
  if ($map.length) {

    let mod = $map.data('mod') || ''
    if (mod !== 'index') {
      ymaps.ready(init)
    }
  }

  function init () {
    const myMap = new ymaps.Map($map[0], {
      center: $map.data('coords'),
      zoom: 15,
      controls: []
    })

    myMap.behaviors.disable('scrollZoom')

    let myPlacemark = new ymaps.Placemark($map.data('coords'), {
      hintContent: '',
      balloonContent: ''
    }, {
      iconImageHref: '/bitrix/templates/softlogic/images/mark.svg',
      iconImageSize: [45, 62]
    })
    myMap.geoObjects.add(myPlacemark)
  }

  const BREAKPOINT_WIDTH = 1200
  const BREAKPOINT_HEIGHT = 600
  const $window = $(window)
  const $logoLink = $('[js-logo-link]', context)
  const $fullPage = $('[js-fullpage]', context)
  const $header = $('[js-header]', context)
  const $headerTop = $('[js-header-top]', context)
  const $headerBottom = $('[js-header-bottom]', context)
  const $headerNav = $('[js-header-nav]', context)
  const $headerNavItems = $headerNav.find('[js-header-nav-link]', context)
  const $globalNav = $('[js-global-nav]', context)
  let resizeTimer
  let DELTA
  let $body = $('body')
  let initMapFullpage
  $body.data('fullpage', false)
  let stateFullPage = false
  let verticalCentered = $window.height() > 1080
  $header.addClass(`header_${1}`)

  $logoLink.on('click.slider', function (e) {
    e.preventDefault()
    $.fn.fullpage.moveTo(1)
  })

  $headerNavItems.on('click.slider', function (e) {
    e.preventDefault()
    $.fn.fullpage.moveTo($(this).data('id'))
  })

  const pages = {
    intro: 1,
    services: 2,
    products: 3,
    projects: 4,
    cases: 5,
    news: 6,
    company: 7,
    contacts: 8
  }

  const initFullPage = () => {
    $fullPage.fullpage({
      autoplay: false,
      dragAndMove: true,
      parallax: true,
      verticalCentered: false,
      afterRender () {
        $body.data('fullpage', true)
      },
      onLeave: function (index, nextIndex, direction) {
        $headerNavItems.removeClass('is-active').filter('[data-id= ' + nextIndex + ']').addClass('is-active')
        $header[0].className = 'header'
        $header.addClass(`header_${nextIndex}`)
        DELTA = 500
        switch (nextIndex) {
          case pages.intro :

            DELTA = index === pages.services && direction === 'up' ? 0 : DELTA

            setTimeout(function () {
              $headerTop.removeClass('is-dark is-simple is-side-light')
              $headerBottom.removeClass('is-social-light is-social-hidden').removeClass('is-simple is-dark')
              $headerNav.removeClass('is-simple is-dark is-hidden')
              $globalNav.removeClass('is-light')
            }, DELTA)
            break
          case pages.services :
            setTimeout(function () {
              $headerTop.removeClass('is-simple is-side-light').addClass('is-dark')
              $headerNav.removeClass('is-hidden').addClass('is-simple is-dark')
              $headerBottom.addClass('is-simple is-dark')
              $globalNav.removeClass('is-light')
            }, DELTA)
            break
          case pages.products :
            $headerTop.removeClass('is-simple is-side-light').addClass('is-dark')
            $headerBottom.removeClass('is-social-light is-social-hidden').addClass('is-simple is-dark')
            $headerNav.addClass('is-simple is-dark')
            $globalNav.removeClass('is-light')
            break
          case pages.projects :
            DELTA = index === pages.cases && direction === 'up' ? 0 : DELTA
            setTimeout(function () {
              $headerTop.removeClass('is-dark is-simple is-side-light')
              $headerBottom.removeClass('is-social-light is-social-hidden').addClass('is-simple').removeClass('is-dark')
              $headerNav.removeClass('is-hidden is-dark')
              $globalNav.addClass('is-light')
            }, DELTA)
            break
          case pages.cases :
            setTimeout(function () {
              $headerTop.addClass('is-dark is-side-light')
              $headerNav.removeClass('is-hidden').addClass('is-dark is-simple')
              $headerBottom.removeClass('is-social-light is-social-hidden').addClass('is-simple is-dark is-social-light')
              $globalNav.removeClass('is-light')
            }, DELTA)
            break
          case pages.news :
            DELTA = index === pages.company && direction === 'up' ? 0 : DELTA
            setTimeout(function () {
              $headerTop.removeClass('is-simple is-side-light').addClass('is-dark')
              $headerNav.addClass('is-dark')
              $headerBottom.removeClass('is-social-light is-social-hidden').addClass('is-simple is-dark')
              $globalNav.removeClass('is-light')
            }, DELTA)
            break
          case pages.company :
            setTimeout(function () {
              $headerTop.removeClass('is-dark is-simple is-side-light')
              $headerNav.removeClass('is-hidden is-dark').addClass('is-simple')
              $headerBottom.removeClass('is-social-light is-dark is-social-hidden').addClass('is-simple')
              $globalNav.addClass('is-light')
            }, DELTA)
            break
          case pages.contacts :
            if (!initMapFullpage) {
              ymaps.ready(init)
              initMapFullpage = true
            }
            setTimeout(function () {
              $headerTop.addClass('is-simple').removeClass('is-dark is-side-light')
              $headerNav.removeClass('is-hidden is-dark').addClass('is-simple')
              $headerBottom.removeClass('is-social-light is-dark').addClass('is-simple is-social-hidden')
              $globalNav.addClass('is-light')
            }, DELTA)
            break
        }
      }
    })
  }
  const destroyFullPage = () => {
    if ($body.data('fullpage')) {
      $headerTop.removeClass('is-dark')
      $headerBottom.removeClass('is-simple is-dark')
      $headerNav.removeClass('is-simple is-dark')
      $.fn.fullpage.destroy('all')
      $body.data('fullpage', false)
    }
  }

  const calculateFullPage = () => {
    if ($window.width() <= BREAKPOINT_WIDTH || $window.height() <= BREAKPOINT_HEIGHT) {
      if ($map.length) {
        ymaps.ready(init)
      }
      destroyFullPage()
    }
    else {
      if ($body.data('fullpage') === false) {
        initFullPage()
      }
    }
  }

  calculateFullPage()
  $window.on('resize', function () {
    calculateFullPage()
  })
})
