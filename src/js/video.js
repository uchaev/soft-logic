app.bind('default', function (context) {
  const $video = $('[js-article-video]', context)
  let videoId

  $video.on('click', function () {
    const $current = $(this)
    videoId = $current.attr('data-video-id')

    const $frame = $(` <iframe width="560" height="315" src="https://www.youtube.com/embed/${videoId}?autoplay=1" allow="autoplay" autoplay="1" allowfullscreen width="960" height="520" frameborder="0"></iframe>`)

    $video.html($frame)
  })
})