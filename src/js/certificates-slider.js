app.bind('default', function (context) {
  $('[js-certificates]', context).each(function () {
    const $certificates = $(this)
    const $certificatesList = $certificates.find('[js-certificates-list]')
    const $certificatesNav = $certificates.find('[js-certificates-nav]')

    $certificatesList.addClass('owl-carousel').owlCarousel({
      loop: false,
      nav: false,
      dots: false,
      navContainer: $certificatesNav,
      margin: 30,
      navText: [],
      items: 4,
      responsive: {
        0: {
          items: 1,
        },
        500: {
          items: 2,
        },
        1000: {
          items: 3
        },
        1200: {
          items: 4
        }
      },
      onInitialized () {
        $certificates.addClass('is-init')
      },
      onChange () {
        $certificatesList.addClass('is-lock')
      },
      onChanged () {
        $certificatesList.removeClass('is-lock')
      }
    })
  })
})