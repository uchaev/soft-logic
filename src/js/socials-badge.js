app.bind('default', function (context) {
  const $badge = $('[js-socials-badge]', context)
  const $icon = $badge.find('[js-socials-badge-icon]', context)
  $icon.on('click', function () {
    $badge.toggleClass('is-open')
  })
})