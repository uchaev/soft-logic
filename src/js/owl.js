app.bind('default', function (context) {
  $('[js-article-list]',context).each(function () {
    const $billboards = $(this);
    const $slider = $billboards.find('[js-article-slider]');

    $slider.addClass('owl-carousel').owlCarousel({
      loop: true,
      dots: false,
      autoplay: true,
      autoplayTimeout: 5000,
      autoplayHoverPause: true,
      touchDrag: true,
      margin: 15,
      autoHeight: true,
      nav: true,
     // navContainer: $('[js-slider-nav]'),
      navText: [],
      items: 1,
      responsive:{
        0:{
          items:1,
        },
        700:{
          items:1,
        },
        1000:{
          items:1
        }
      },
      onInitialized() {
        $billboards.addClass('is-init');
      }
    })
  })
});