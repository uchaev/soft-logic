app.bind('default', function (context) {
  $('[js-products-slider]', context).each(function () {
    const $slider = $(this)
    const $item = $slider.find('[js-products-slider-item]')
    const $slide = $slider.find('[js-products-slide]')
    $item.on('click', function () {
      $slide.add($item).removeClass('is-active')
      $slide.filter('[data-id=' + $(this).data('id') + ']').add($(this)).addClass('is-active')
    })
  })
})