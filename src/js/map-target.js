app.bind('default', function (context) {
  $('[js-map-point]', context).each(function () {
    const $root = $(this)
    $root.on('click', function () {
      $.scrollTo($('[js-map-target=' + $root.data('target') + ']'), 300, { offset: { top: -60 } })
    })
  })
})