app.bind('default', function (context) {

  const $slider = $('[process-slider]', context)

  $slider.addClass('owl-carousel').owlCarousel({
    loop: false,
    nav: false,
    dots: false,
    margin: 30,
    autoHeight: true,
    navText: [],
    items: 4,
    responsive: {
      0: {
        items: 1,
      },
      500: {
        items: 2,
      },
      1000: {
        items: 4
      }
    },
  })

})