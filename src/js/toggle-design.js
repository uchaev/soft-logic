app.bind('default', function (context) {

    $('[toggle-design-btn]').click(function(){
        let type = $(this).data('design')
        $(this).parents('[toggle-design-btns]').find('.active[toggle-design-btn]').removeClass('active')
        $(this).addClass('active')
        $(this).parents('[toggle-design]').find('.active[toggle-design-tab]').removeClass('active')
        $(this).parents('[toggle-design]').find('[toggle-design-tab][data-design='+type+']').addClass('active')
   
        $("[com-slider]").trigger('reinit.twentytwenty')

        
    })
    $('[type-design-btn]').click(function(){
        let type = $(this).data('des')
        $(this).parents('[type-design-btns]').find('.active[type-design-btn]').removeClass('active')
        $(this).addClass('active')
        $(this).parents('[toggle-design-tab]').find('.active[type-design-tab]').removeClass('active')
        $(this).parents('[toggle-design-tab]').find('[type-design-tab][data-des='+type+']').addClass('active')
   
        $("[com-slider]").trigger('reinit.twentytwenty')

    })
})