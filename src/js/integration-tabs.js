app.bind('default', function (context) {

    $('[js-toggle-tab-btn]', context).click(function(){
        let type = $(this).data('tab')
        $(this).parents('[js-toggle-tabs-btns]').find('.active').removeClass('active')
        $(this).addClass('active')
        $(this).parents('[js-toggle-tabs]').find('.active[js-toggle-tab]').removeClass('active')
        $(this).parents('[js-toggle-tabs]').find('[js-toggle-tab][data-tab='+ type +']').addClass('active')
        
    })

})