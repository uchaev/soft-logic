app.bind('default', function (context) {
    $('[js-queue-slider]', context).each(function () {
        const $slider = $(this)
        const $item = $slider.find('[js-queue-slider-title]')
        const $slide = $slider.find('[js-queue-slider-tab]')
        const $slidePrev = $slider.find('[js-queue-slider-prev]')
        const $slideNext = $slider.find('[js-queue-slider-next]')

        const totalPages = $slider.data('pages')
        const changeSlide = (id) => {
            $slide.add($item).removeClass('is-active')
            $slide.filter('[data-id=' + id + ']').add($item.filter('[data-id=' + id + ']')).addClass('is-active')
        };

        const handlerArrow = (direction = 'next') => {
            const current = $item.filter('.is-active').data('id')

            if (direction === 'next') {
                if ($item.filter('.is-active').data('id') === totalPages) {
                    changeSlide(1)

                } else {
                    changeSlide(current + 1)

                }
            } else {
                if ($item.filter('.is-active').data('id') === 1) {
                    changeSlide(totalPages)

                } else {
                    changeSlide(current - 1)

                }
            }
        };
        $item.on('click', function () {
            changeSlide($(this).data('id'))
        });
        $slidePrev.on('click', function () {
            handlerArrow('prev')
        })
        $slideNext.on('click', function () {
            handlerArrow()
        })
    })
})


app.bind('default', function (context) {
    $('[js-queue-content-slider]', context).each(function () {
        const $slider = $(this)
        const $slide = $slider.find('[js-queue-content-slider-tab]')
        const $slidePrev = $slider.find('[js-queue-content-slider-prev]')
        const $slideNext = $slider.find('[js-queue-content-slider-next]')
        const $counter = $slider.find('[js-queue-content-slider-counter]')

        const totalPages = $slider.data('pages')
        const changeSlide = (id) => {
            $slide.removeClass('is-active')
            $slide.filter('[data-id=' + id + ']').addClass('is-active')
            $counter.html(id)
        };

        const handlerArrow = (direction = 'next') => {
            const current = $slide.filter('.is-active').data('id')
            if (direction === 'next') {
                if ($slide.filter('.is-active').data('id') === totalPages) {
                    changeSlide(1)

                } else {
                    changeSlide(current + 1)

                }
            } else {
                if ($slide.filter('.is-active').data('id') === 1) {
                    changeSlide(totalPages)

                } else {
                    changeSlide(current - 1)

                }
            }
        };
        $slidePrev.on('click', function () {
            handlerArrow('prev')
        })
        $slideNext.on('click', function () {
            handlerArrow()
        })
    })
})
