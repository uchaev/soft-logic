app.bind('default', function (context) {
  const $body = $('.body', context)
  const $bodyWrapper = $body.find('.body__wrapper')

  $('[js-menu-toggle]', context).each(function () {

    const $menuOpener = $(this)
    const target = $menuOpener.data('target')
    const $menuTarget = $(`[js-menu][data-target=${target}]`, context)
    const $menuTargetWrapper = $menuTarget.find('[js-menu-wrapper]')

    $menuTargetWrapper.on('click', function (e) {
      e.stopPropagation()
    })

    $menuTarget.on('click', function () {
      $bodyWrapper.removeClass('is-locked')
      $menuTarget.removeClass('is-open')
      if ($body.data('fullpage')) {
        $.fn.fullpage.setMouseWheelScrolling(true)
        $.fn.fullpage.setAllowScrolling(true)
      }
    })
    $menuOpener.on('click', function () {
      $menuTarget.addClass('is-open')
      $bodyWrapper.addClass('is-locked')

      if ($body.data('fullpage')) {
        $.fn.fullpage.setMouseWheelScrolling(false)
        $.fn.fullpage.setAllowScrolling(false)
      }
    })
  })

  const $linkChildren = $('[js-menu-link-children]', context)

  $linkChildren.on('mouseover', function () {
    const id = $(this).data('id')
    $('[js-menu-grouped-list]').removeClass('is-active')
    $(`[js-menu-grouped-list][data-id=${id}]`).addClass('is-active')
  })

  const $menuCloser = $('[js-menu-close]', context)

  $menuCloser.on('click', function () {
    $(`[js-menu][data-target=${$(this).data('target')}]`, context).removeClass('is-open')
    $bodyWrapper.removeClass('is-locked')
    if ($body.data('fullpage')) {
      $.fn.fullpage.setMouseWheelScrolling(true)
      $.fn.fullpage.setAllowScrolling(true)
    }
  })

  const $linkMenu = $('.menu-nav__link ')
  $linkMenu.on('click', function () {
    $menuCloser.trigger('click')
  })
})