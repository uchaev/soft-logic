app.bind('default', function (context) {
    const $agreement = $('[js-agreement]');
    const $agreementBtn = $agreement.find('[js-agreement-btn]');
    localStorage.getItem('agreement');
    if (!localStorage.getItem('agreement')) {
        $agreement.addClass('is-visible')
    }
    $agreementBtn.on('click', function () {
        $agreement.remove();
        localStorage.setItem('agreement', 'true')
    })
});

