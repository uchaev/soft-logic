app.bind('default', function (context) {
  $('[js-notification]', context).each(function () {
    const $notification = $(this)
    const $notificationClose = $notification.find('[js-notification-close]')
    $notificationClose.on('click.notification', () => {
      $notification.remove()
    })
  })
})