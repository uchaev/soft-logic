app.bind('default', function (context) {
    $('[js-bank-advantage-slider]', context).each(function () {
        const $articles = $(this)
        const $slider = $articles.find('[js-bank-advantage-slider-list]')
    
        $slider.addClass('owl-carousel').owlCarousel({
            loop: false,
            nav: false,
            dots: false,
            margin: 20,
            navText: [],
            items: 4,
            responsive: {
            0: {
                items: 1,
            },
            500: {
                items: 2,
            },
            1000: {
                items: 4
            }
            },
            onInitialized () {
                $articles.addClass('is-init')
            },
            onChange () {
                $slider.addClass('is-lock')
            },
            onChanged () {
                $slider.removeClass('is-lock')
            }
        })
  
    })
})