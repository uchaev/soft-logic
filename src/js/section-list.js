app.bind('default', function (context) {
    let $nav = $('[js-section-list-nav]', context);
    let $next = $nav.find('[js-section-list-nav-next]');
    let $prev = $nav.find('[js-section-list-nav-prev]');
    let $count = $('[js-section-count]', context);
    let $countCurrent = $('[js-section-count-current]', context);
    $next.on('click', function () {
        changeSlide()
    });
    $prev.on('click', function () {
        changeSlide('prev')
    });

    const $sectionItem = $('[js-section-list-item]', context);
    $count.html($sectionItem.length);
    const changeSlide = (directon = 'next') => {
        let $changeItem;
        let $active = $('[js-section-list-item].is-active');
        if (directon === 'next') {
            $changeItem = $active.next().length > 0 ? $active.next() : $sectionItem.eq(0)
        }
        if (directon === 'prev') {
            $changeItem = $active.prev().length > 0 ? $active.prev() : $sectionItem.eq($sectionItem.length - 1)
        }
        $('[js-section-list-text-item]').removeClass('is-active');
        $('[js-section-list-text-item]').filter('[data-id=' + $changeItem.data('id') + ']').addClass('is-active');
        $sectionItem.removeClass('is-active');
        $changeItem.addClass('is-active');
        $countCurrent.html($changeItem.data('id'))
    }
});
