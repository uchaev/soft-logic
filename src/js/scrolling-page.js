app.bind('default', function (context) {
  $('[js-scrolling-page]', context).each(function () {
    const $scrollingPage = $(this)
    const $scrollingPageItems = $scrollingPage.find('[js-scrolling-item ]')
    const $scrollingPageNav = $scrollingPage.find('[js-scrolling-nav]')
    const $scrollingPageNavPrev = $scrollingPage.find('[js-scrolling-nav-prev]')
    const $scrollingPageNavNext = $scrollingPage.find('[js-scrolling-nav-next]')
    const maxSlide = $scrollingPageItems.length
    let currentSlide = $scrollingPageNav.data('current-slide')

    let timer

    $scrollingPageNavNext.on('click', function () {
      clearTimeout(timer)
      $scrollingPageNavPrev.removeClass('is-lock')
      if (currentSlide < maxSlide - 1) {
        currentSlide += 1
        let $currentSlide = $scrollingPageItems.filter('[data-id=' + currentSlide + ']')
        $currentSlide.addClass('is-hidden').next().find('.steps-item').addClass('is-active')
      }
      if (currentSlide === maxSlide - 1) {
        $scrollingPageNavNext.addClass('is-lock')
      }

      timer = clearTime()

    })
    $scrollingPageNavPrev.on('click', function () {
      clearTimeout(timer)
      $scrollingPageNavNext.removeClass('is-lock')
      if (currentSlide > 0) {
        let $currentSlide = $scrollingPageItems.filter('[data-id=' + currentSlide + ']')
        $currentSlide.removeClass('is-hidden').next().find('.steps-item').removeClass('is-active')
        currentSlide -= 1
      }
      if (currentSlide === 0) {
        $scrollingPageNavPrev.addClass('is-lock')
      }
      timer = clearTime()
    })

    function clearTime () {
      return setTimeout(() => {
        clear()
      }, 3000)
    }

    function clear () {
      currentSlide = 0
      $scrollingPageNavPrev.addClass('is-lock')
      $scrollingPageNavNext.removeClass('is-lock')
      $scrollingPageItems.removeClass('is-hidden').find('.steps-item').removeClass('is-active')
      $scrollingPageItems.eq(0).find('.steps-item').addClass('is-active')
    }

    $('[js-nav-tabs]', context).on('click.scroll', function () {
      clear()
    })
  })
})