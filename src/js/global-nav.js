app.bind('default', function (context) {
  const $globalNav = $('[js-global-nav]', context)
  const $globalNavPrev = $globalNav.find('[js-global-nav-prev]')
  const $globalNavNext = $globalNav.find('[js-global-nav-next]')
  $globalNavPrev.on('click', function () {
    changeNav($('.screen.active').data('anchor'), 'prev')

  })
  $globalNavNext.on('click', function () {
    changeNav($('.screen.active').data('anchor'), 'next')
  })

  function changeNav (id, directon) {
    switch (id) {
      case 'services':
        navs()
        break
      case 'projects':
        navs()
        break
      case 'cases':
        navs()
        break
      case 'company':
        navs()
        break
      case 'news':
        slider()
        break
      case 'products':
        slider()
        break
    }

    function navs () {
      const $screen = $('.screen.active')
      const $activeMenuItem = $screen.find('[js-nav-tabs-item].is-active').closest('.nav-simple__item')
      const $menuItems = $screen.find('.nav-simple__item')
      let $changeItem

      if (directon === 'next') {
        $changeItem = $activeMenuItem.next().length > 0 ? $activeMenuItem.next().find('[js-nav-tabs-item]') : $menuItems.eq(0).find('[js-nav-tabs-item]')
      }
      if (directon === 'prev') {
        $changeItem = $activeMenuItem.prev().length > 0 ? $activeMenuItem.prev().find('[js-nav-tabs-item]') : $menuItems.eq($menuItems.length - 1).find('[js-nav-tabs-item]')
      }

      $changeItem.trigger('click')
    }

    function slider () {
      const $screen = $('.screen.active')
      let $btn
      if (directon === 'next') {
        $btn = $screen.find('.owl-next')
      }
      if (directon === 'prev') {
        $btn = $screen.find('.owl-prev')
      }
      $btn.trigger('click')
    }
  }
})