'use strict'
app.bind('default', function (context) {
  $('[js-callback-tile]', context).each(function () {
    const $callback = $(this)
    const $form = $callback.find('[js-callback-tile-form]')
    let timer1 = void 0
    let timer2 = void 0
    $form.on('submit', function (e) {
      console.log(e)
      e.preventDefault()
      const $helps = $form.find('.help-block')
      let count = 0
      for (let i = 0; i < $helps.length; i++) {
        if ($($helps[i]).attr('data-bv-result') !== 'VALID') {
          count++
        }
      }
      if (count > 1) {
        return
      }

      clearTimeout(timer1)
      clearTimeout(timer2)

      $callback.addClass('is-loading')
      timer1 = setTimeout(function () {
        $callback.addClass('is-message-success').removeClass('is-loading')
        $form.trigger('reset')
        timer2 = setTimeout(function () {
          $callback.removeClass('is-message-success is-message-error')
        }, 3000)
      }, 1000)
    })
  })
})
