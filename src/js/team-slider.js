app.bind('default', function (context) {
  $('[js-team-slider]', context).each(function () {
    const $slider = $(this)
    const $sliderList = $slider.find('[js-team-list]')
    const $sliderListItem = $sliderList.find('[js-team-list-item]')
    const $sliderNav = $slider.find('[js-team-nav]')
    const $sliderNavNext = $sliderNav.find('[js-team-next]')
    const $sliderNavPrev = $sliderNav.find('[js-team-prev]')

    $sliderNavNext.on('click', function () {
      changeSlide('next')
    })

    $sliderNavPrev.on('click', function () {
      changeSlide('prev')
    })

    const changeSlide = (direction) => {
      let $changeItem
      let $activeItem = $sliderListItem.filter('.is-active')
      switch (direction) {
        case 'next':
          $changeItem = $activeItem.next().length > 0 ? $activeItem.next() : $sliderListItem.eq(0)
          break
        case 'prev':
          $changeItem = $activeItem.prev().length > 0 ? $activeItem.prev() : $sliderListItem.eq($sliderListItem.length - 1)
          break
      }

      $sliderListItem.removeClass('is-active')
      $changeItem.addClass('is-active')
    }
  })
})