app.bind('default files', function (context) {

    const $filesCounter = $('[js-files-counter]', context);
    const $filesAmount = $filesCounter.find('[js-files-amount]');
    let amound =  $filesCounter.data('count');
    const $filesBox = $('[js-files-box]', context);

    $('[js-files]', context).each(function () {

        const $file = $(this);
        const $input = $file.find('[js-files-input]');
        const $inputTitle = $file.find('[js-files-title]');
        const $btnClose = $file.find('[js-files-close]');
        const mod = $(this).data('mod');
        $btnClose.on('click', function () {
            const $root = $(this);
            const $input = $root.closest('[js-files]');

            if (mod === 'single') {
                $input.removeClass('is-filled is-load');
                $input.find('[js-files-title]').html('Ваше резюме');
                $input.find('input').val('');
                return;
            }

            if ($root.data('type') === 'image') {
                let $imput = $(`<input type="text" name="image_delete-id[]" hidden value='${$root.data('id')}'>`);
                $root.data('type','close');
                $filesBox.append($imput);
            }

            if (amound === 5) {
                $input.removeClass('is-filled is-load');
                $input.find('[js-files-title]').html('Добавить фото');
                $input.find('input').val('');
            }
            else  {
                $input.remove();
            }
            amound = amound - 1;
            $filesCounter.data('count',amound);
            $filesAmount.html(amound);
            $filesCounter.css('color','black');
        });

        $input.off('.files').on('change.files', function () {
            const $root = $(this);
            const $inputTemplate = $root.closest('[js-files]').clone(true);
            const arr = $root[0].files;
            const $input = $root.closest('[js-files]');
            if (arr.length) {
                $input.addClass('is-filled');
                $input.find('[js-files-title]').html(arr[0].name);
                amound = amound + 1;
                $filesCounter.data('count',amound);
                $filesAmount.html(amound);
                if (amound < 5) {
                    $filesBox.append($inputTemplate);
                }
            }
            else {
                $input.removeClass('is-filled');
                $input.find('[js-files-title]').html('Добавить фото');
            }
            $filesCounter.css('color','black');
        });
    });
});
